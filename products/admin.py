from django.contrib import admin
from products.models import Product, ProductSize

admin.site.register(ProductSize)

admin.site.register(Product)