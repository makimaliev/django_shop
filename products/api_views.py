from rest_framework import viewsets
from .models import Product 
from .serializers import ProductSerializer
from rest_framework.response import Response

'''
class ProductViewSet(viewsets.ModelViewSet):
    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer

    def list(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({'products': serializer.data})
'''

class ProductViewSet(viewsets.ViewSet):
    
    def list(self, request):
        queryset = Product.objects.all()
        serializer = ProductSerializer(queryset, many=True)
        return Response({'products': serializer.data})