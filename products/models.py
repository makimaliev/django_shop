from django.db import models
from django.contrib.postgres.fields import ArrayField

class ProductSize(models.Model):
    name = models.CharField(max_length=3)

    def __str__(self):
        return self.name

class Product(models.Model):
    sku = models.BigIntegerField(null=False)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    style = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    installments = models.IntegerField()
    currency = models.CharField(max_length=5)
    currency_format = models.CharField(max_length=2)
    is_free_shipping = models.BooleanField()
    available_sizes = models.ManyToManyField(ProductSize)