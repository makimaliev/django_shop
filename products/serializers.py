from rest_framework import serializers

from . import models

class ProductSerializer(serializers.ModelSerializer):
    availableSizes = serializers.StringRelatedField(source='available_sizes', many=True)
    currencyId = serializers.CharField(source='currency')
    currencyFormat = serializers.CharField(source='currency_format')
    isFreeShipping = serializers.BooleanField(source='is_free_shipping')

    class Meta:
        model = models.Product
        fields = ('id', 'sku', 'title','description','style','price','installments','currencyId','currencyFormat','isFreeShipping', 'availableSizes')