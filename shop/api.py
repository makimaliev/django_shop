from rest_framework import routers
from products import api_views

router = routers.DefaultRouter()
router.register(r'products', api_views.ProductViewSet, basename='product')